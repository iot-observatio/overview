# IoTo Overview
  
This project is overview of the projects for the IoTo infrastucture.


## Getting started
Please follow [the documentation](https://www.hackster.io/projects/5ea27e) for the background details to get an idea of the big picture.


## Projects for the IoTo infrastucture
Please work in order for easy living:
1. [IoTo IoT Measuring Device](https://gitlab.com/iot-observatio/ioto-iot-measuring-device)
1. [IoTo Ruuvi to MQTT](https://gitlab.com/iot-observatio/ioto-ruuvi-to-mqtt)
1. [IoTo MQTT to Google Cloud Services](https://gitlab.com/iot-observatio/ioto-ruuvi-to-mqtt)


## All things done?
Good job, well done!
